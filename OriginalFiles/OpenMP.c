#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>

#define CERT 1
#define FALS 0

// Ha de ser inicialment correcta !!
int taulaOriginal[9][9] = \
{1,2,3, 4,5,6,  7,8,9,  \
        9,8,7, 3,2,1,  6,5,4,  \
        6,5,4, 7,8,9,  1,2,3,  \
        \
        7,9,8, 1,3,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0,  \
        \
        0,0,0, 0,0,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0};

int puc_posar(int x, int y, int z, int (*taula)[9])
{
	int i,j,pi,pj;
    
	for (i=0;i<9;i++)
	{
		if (taula[x][i] == z) return(FALS); // Files
		if (taula[i][y] == z) return(FALS); // Columnes
	}
	
	// Quadrat
	pi = (x/3)*3; //truncament
	pj = y-y%3; //truncament
	for (i=0;i<3;i++) 
		for (j=0;j<3;j++) 
        if (taula[pi+i][pj+j] == z) return(FALS);
    
	return(CERT);
}

////////////////////////////////////////////////////////////////////
int recorrer(int i, int j, int (*taula)[9])
{
    int k;
    long int s=0;
    
    if (taula[i][j]) //Valor fixe no s'ha d'iterar
    {
        if (j<8) return(recorrer(i,j+1,taula));
        else if (i<8) return(recorrer(i+1,0,taula));
        else return(1); // Final de la taula
    }
    else // hi ha un 0 hem de provar
    {
        for (k=1;k<10;k++)
            if (puc_posar(i,j,k,taula)) 
        {
            taula[i][j]= k; 
            if (j<8) s += recorrer(i,j+1,taula);
            else if (i<8) s += recorrer(i+1,0,taula);
            else s++;
            taula[i][j]= 0;
        }
    }
    return(s);
}

int splitData()
{
	int k,l,i,j;
	int m, n, s=0;
	int P[3][2];
	int nPunts=0;
	for (i=0;(i<9)&&(nPunts<3);i++) 
		for (j=0;(j<9)&&(nPunts<3);j++)
    {
        if (!taulaOriginal[i][j])
        {
            P[nPunts][0]=i;
            P[nPunts][1]=j;
            nPunts++;
        }
    }
    
#pragma omp parallel default(none) private(k,m,n) shared(P,taulaOriginal) reduction(+:s)
	{
		s=0;
		int taula[9][9];
		memcpy(taula, taulaOriginal, 9*9*sizeof(int));
		
		for (k=1;k<10;k++)
		{
			if(puc_posar(P[0][0], P[0][1], k, taula))
			{
				taula[P[0][0]][P[0][1]]=k;
				for (m=1;m<10;m++)
				{
					if(puc_posar(P[1][0], P[1][1], m, taula))
					{
						taula[P[1][0]][P[1][1]]=m;
#pragma omp for schedule(dynamic, 1) nowait
						for (n=1;n<10;n++)
						{
							if(puc_posar(P[2][0], P[2][1], n, taula))
							{
								taula[P[2][0]][P[2][1]]=n;
								if (P[2][1]<8)  	s += recorrer(P[2][0],   P[2][1]+1, taula);
								else if (P[2][0]<8) s += recorrer(P[2][0]+1, 0, taula);
								else s++;
								taula[P[2][0]][P[2][1]]=0;
							}
						}
						taula[P[1][0]][P[1][1]]=0;
					}
				}
				taula[P[0][0]][P[0][1]]=0;
			}
		}
	}
	return(s);
}

////////////////////////////////////////////////////////////////////
int main()
{
    int i,j,k;
    long int nsol;
    
    nsol = splitData();
    printf("\nnumero solucions : %ld\n",nsol);
    exit(0);
}
