#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <mpi.h>

#define TRUE 1
#define FALSE 0

#define MAX_NIVELL 7
#define TAG 0

int nProc, id, nivell = 0;
int * results, * values;
MPI_Request * requests;
MPI_Request request;
MPI_Status status;

// Ha de ser inicialment correcta !!
int taula[9][9] = \
{1,2,3, 4,5,6,  7,8,9,  \
        9,8,7, 3,2,1,  6,5,4,  \
        6,5,4, 7,8,9,  1,2,3,  \
        \
        7,9,8, 1,3,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0,  \
        \
        0,0,0, 0,0,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0,  \
        0,0,0, 0,0,0,  0,0,0};


int puc_posar(int x, int y, int z)
{
	int i, j, pi, pj;
    
	for (i=0;i<9;i++)
	{
		if (taula[x][i] == z) return(FALSE); // Files
		if (taula[i][y] == z) return(FALSE); // Columnes
	}
    
	// Quadrat
	pi = (x/3)*3; //truncament
	pj = y-y%3; //truncament
	for (i=0;i<3;i++)
		for (j=0;j<3;j++)
        if (taula[pi+i][pj+j] == z) return(FALSE);
    
	return(TRUE);
}

////////////////////////////////////////////////////////////////////
int recorrer(int i, int j)
{
	int k;
	long int s=0;
    
	if (taula[i][j]) //Valor fixe no s'ha d'iterar
	{
		if (j<8) return(recorrer(i,j+1));
		else if (i<8) return(recorrer(i+1,0));
		else return(1); // Final de la taula
	}
	else // hi ha un 0 hem de provar
	{
        for (k=1;k<10;k++)
            if (puc_posar(i,j,k))
		{
			taula[i][j]= k;
			if (j<8) s += recorrer(i,j+1);
			else if (i<8) s += recorrer(i+1,0);
			else s++;
			taula[i][j]= 0;
		}
	}
	return(s);
}

int recorrerArrel(int i, int j, int nivell)
{
	int k, p, count, dst_id;
	long int s=0;
    
	if (taula[i][j]) //Valor fixe no s'ha d'iterar
	{
		if (j<8) return(recorrerArrel(i, j+1, nivell));
		else if (i<8) return(recorrerArrel(i+1, 0, nivell));
		else return(1); // Final de la taula
	}
	else // hi ha un 0 hem de provar
	{
        for (k=1;k<10;k++)
            if (puc_posar(i,j,k))
		{
			values[nivell] = taula[i][j] = k;
			nivell++;
            
			if (nivell == MAX_NIVELL)
			{
                
				count = 0;
				for (p = 1; p < nProc; p++)
				{
					MPI_Test(&requests[p], &count, &status);
					if (count)
					{
						dst_id = p;
						break;
					}
				}
                
				if(count) //Hay threads libres
				{
					//printf("0: sending job to %d\n", dst_id);
					s += results[dst_id];
					MPI_Send(values, MAX_NIVELL, MPI_INT, dst_id, TAG, MPI_COMM_WORLD);
					MPI_Irecv(&results[dst_id], 1, MPI_INT, dst_id, TAG, MPI_COMM_WORLD, &requests[dst_id]);
				}
				else //No hay threads libres
				{
					//printf("0: everyone is busy\n");
                    
					if (j<8) s += recorrer(i, j+1);
					else if (i<8) s += recorrer(i+1, 0);
					else s++;
				}
			}
			else if (j<8) s += recorrerArrel(i, j + 1, nivell);
			else if (i<8) s += recorrerArrel(i + 1, 0, nivell);
			else s++;
			nivell--;
			values[nivell] = taula[i][j] = 0;
		}
	}
	return(s);
}

void recorrerAltres()
{
	int zeros[MAX_NIVELL+1][2];
	int i, j, k = 0;
    
	for(i=0;i<9;i++)
		for(j=0;j<9;j++)
        if(taula[i][j] == 0)
    {
        zeros[k][0] = i;
        zeros[k][1] = j;
        k++;
        if(k == MAX_NIVELL+1)
            goto sortir;
    }
	sortir:;
    
	int s=0, recieved = 0;
	for(;;)
	{
		while(!recieved)
			MPI_Test(&request, &recieved, &status);
        
		recieved=0;
        
		//printf("%d: got a job: %d \n", id,values[0]);
		if (values[0]) // Modificar sudoku con nueva informacion
		{
			for(i=0;i<MAX_NIVELL;i++)
				taula [zeros[i][0]] [zeros[i][1]] = values[i];
		}
		else
		{ //si values[0] val 0 s'ha acabat
			MPI_Send(&s, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD); //Enviar Sumatorio
			s=0;
			break;
		}
		MPI_Send(&s, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD); //Enviar Sumatorio
		MPI_Irecv(values, MAX_NIVELL, MPI_INT, 0, TAG, MPI_COMM_WORLD, &request); // Siguiente peticion de tarea
        
		s = recorrer(zeros[MAX_NIVELL][0], zeros[MAX_NIVELL][1]);
        
		//printf("%d: waiting for job...\n", id);
	}
}
////////////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
	int i, j, k, aux;
	long int nsol;
    
	MPI_Init(&argc, &argv);
    
	MPI_Comm_size(MPI_COMM_WORLD,&nProc);
	MPI_Comm_rank(MPI_COMM_WORLD,&id);
	
	if (nProc == 1)
	{
		nsol = recorrer(0, 0);
		printf("numero solucions : %ld\n", nsol);
		MPI_Finalize();
		return(0);
	}
    
	values = malloc(sizeof(int) * MAX_NIVELL);
    
	if(!id)
	{
		requests = malloc(sizeof(MPI_Request) * nProc);
		results = malloc(sizeof(int) * nProc);
		for (i=0; i<nProc;i++)
			MPI_Irecv(&results[i], 1, MPI_INT, i, TAG, MPI_COMM_WORLD, &requests[i]);
        
		//espera a que s'inicialitzi el thread1
		int recieved = 0;
		while(!recieved)
			MPI_Test(&requests[1], &recieved, &status);
        
		nsol = recorrerArrel(0, 0, 0);
        
		for (j=0; j<2; j++)	// Obtener los dos ultimos mensajes del resto de threads
		{
			i=1;
			while(i<nProc)
			{
				MPI_Test(&requests[i], &aux, &status);
                
				if (aux) // si request[i] esta completada
				{
                    nsol += results[i];
                    MPI_Send(values, MAX_NIVELL, MPI_INT, i, TAG, MPI_COMM_WORLD); //enviar missatge per acabar thread
                    MPI_Irecv(&results[i], 1, MPI_INT, i, TAG, MPI_COMM_WORLD, &requests[i]);
                    i++;
				}
			}
		}
        
		printf("numero solucions : %ld\n", nsol);
	}
	else
	{
		aux = 0;
		MPI_Send(&aux, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD);
		MPI_Irecv(values, MAX_NIVELL, MPI_INT, 0, TAG, MPI_COMM_WORLD, &request);
		recorrerAltres();
	}
    
	MPI_Finalize();
	exit(0);
}
