# Sudoku Solutions

Class assignment where we were given a code in C that computes the number of solutions of a specific Sudoku board and we had to apply parallelism to it.

The original solution was written in C and parallelized using OpenMP and OpenMPI.

NOTE: original code and results were written in Catalan and Spanish.